# Software Setup

## Install Windows Subsystem for Linux (WSL)

### This is a system that allows you to run linux directly on windows

1. Open a Powershell admin access by right clicking on the Windows logo in the bottom right of the screen. ![alt text](assets/img/powershell_admin.gif
 "Powershell")
2. WARNING the next step will restart your computer save all your work.
3. Copy the following command to into the powershell you just opened
    ```Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux```
4. After the reboot  and click this [link](https://www.microsoft.com/en-us/p/ubuntu-1804-lts/9n9tngvndl3q?activetab=pivot:overviewtab) which will take you to the install page of Ubuntu 18.04 LTS ![alt text](assets/img/ubuntu.png)
5. Open the Ubuntu 18.04 app it will take a couple of minutes to setup and then it will ask for a username and password make sure you set it to something you can remember. **Warning When inputting the password there will be no feedback it is working linux just does not show any password indications**

## Install VcxSRV

### This will allow you to use some gui based applications on wsl

1. Click the [link](https://sourceforge.net/projects/vcxsrv/files/latest/download) to download the installer
2. Install the program (it will be called xlaunch after install)
3. Open a WSL terminal by searching for Ubuntu 18.04
4. Type the two following commands this will allow wsl to communicate with VCXsrv
    ```echo export DISPLAY=:0 >> ~/.bashrc```
    ```source ~/.bashrc```
5. Search for XLaunch and run it
6. Select multiple windows and click next  
![alt text](assets/img/xlaunch_multiple.png)
7. Select no client and click next  
![alt text](assets/img/xlaunch_noclient.png)
8. Un-check the Native opengl check box and click next  
![alt text](assets/img/xlaunch_opengl.png)
9. Click Finish

## Setup WSL

### This section goes over how to setup git, ssh keys for gitlab, as well as getting your packages up to date

1. Open WSL by searching for Ubuntu 18.04
2. To update you packages type the following command  

    ```sudo apt-get update && sudo apt-get upgrade```

3. Type the following commands replacing the Your name and Your Email with your name and your email. this sets the identity your commits will have
    - ```git config --global user.name “Your Name”```
    - ```git config --global user.email “Your Email”```
4. Run the following command to install xclip which is used in the ssh key install process
    ```sudo apt-get install xclip```
5. To setup ssh keys you will need a gitlab account once you have an account please contact Tim or Ben in the discord to get added to the gitlab team
6. Click the user name in the upper right hand corner of the gitlab window.  
![alt text](assets/img/gitlab_setting.png "Settings")
7. Click SSH Keys in the menu on the left  
![alt text](assets/img/gitlab_ssh.png "SSH")
8. In a new tab follow this [link](https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair)
9. Use the ED25519 instructions to generate a key
10. Once you have the key in you clipboard navigate back to the ssh keys page and paste it into the field labeled ssh key
11. Name the ssh key and save

## Installing ROS 1 and 2

### This will install ROS 1 and 2 by default ROS 1 will be sourced

**WARNING** this takes while as much as a couple of hours depending on your computer and network

1. In the wsl window run the following commands
    ```cd /mnt/c/```
    ```mkdir dev && cd dev```
    ```git clone git@gitlab.com:KSU_EVT/learning/software_setup.git```
    ```cd software_setup/scripts && chmod +x install_ros.sh```
    ```./install_ros.sh melodic```

This means that the repository you just cloned is under the C drive in the dev folder

## Installing and configuring Vscode  

![alt text1][logo]

### The text editing experience is up to the user if you want use Atom or any other editor thats fine but we will only support setups in vscode

1. Click this [link](https://code.visualstudio.com/) and download VScode
2. Follow install process
3. Open Vscode and select "Open Folder" from the file menu.
4. Select the software_setup folder that was cloned earlier.
5. Click on the extensions icon ![alt text](assets/img/extensions.PNG)
6. Type @recommended and click the cloud icon.  
![alt text](assets\img\extensions_install.png)
7. After the extensions finish installing and the window is reloaded click the green icon in the bottom left corner  and select Remote-WSL: Reopen Folder in WSL  
![alt text](assets/img/remote_open.gif)
8. Then select Install All from the popup

[logo]: assets/img/vscode.jpg "Title"
